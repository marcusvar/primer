/*
 * File: app.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

// @require @packageOverrides
Ext.Loader.setConfig({
    enabled: true,
    paths: {
        'Ext.ux': './app/ux'
    }
});


Ext.Loader.setPath('Ext.ux.CpfField','packages/CpfField/src/CpfField.js');

Ext.application({

    requires: [
        'Ext.Loader',
        'Ext.ux.InputTextMask',
        'Ext.ux.TextMaskPlugin',
        'Ext.ux.upload.Button',
        'Ext.ux.upload.plugin.Window',
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.util.*',
        'Ext.state.*',
        'Ext.ux.DataView.DragSelector',
        'Ext.layout.container.Border',
        'Ext.layout.container.Accordion',
        'Ext.layout.container.Column',
        'Ext.Action'
    ],
    models: [
        'mdlUsuario',
        'mdlRegiaoUF',
        'mdlLocalidades',
        'mdlOferta',
        'mdlUF',
        'mdlImagem',
        'mdlSugestao',
        'mdlComentario',
        'mdlRelato'
    ],
    stores: [
        'strUsuario',
        'strUF',
        'strLocalidade',
        'strOferta',
        'strCidade',
        'strRegiaoUF',
        'strImagem',
        'strSugestao',
        'strComentario',
        'strRelato'
    ],
    views: [
        'wndAcesso',
        'vpPrincipal',
        'pnlUsuarios',
        'gridUsuarios',
        'formUsuarios',
        'wndSobre',
        'pnlOfertas',
        'pnlTrocarSenha',
        'gridOfertas',
        'formOfertas',
        'wndImagem'
    ],
    controllers: [
        'ctrlLogin',
        'ctrlGeral',
        'ctrlMenu',
        'ctrlUsuarios',
        'ctrlTrocarSenha',
        'ctrlOfertas',
        'ctrlImagens',
        'ctrlSugestao',
        'ctrlComentario',
        'ctrlRelato'
    ],
    name: 'Primer',

    launch: function() {
        Ext.Ajax.request({
            url: "data/checkSession.php",
            success: function(response) {
                if (Ext.decode(response.responseText).success) {
                    global.id = Ext.decode(response.responseText).Id;
                    global.funcao = Ext.decode(response.responseText).Funcao;
                    global.login = Ext.decode(response.responseText).User;
                    global.data = new Date(Ext.decode(response.responseText).Data);
                    Ext.create("Primer.view.vpPrincipal");
                    Ext.getStore('strUF').load();
                } else {
                    Ext.create("Primer.view.wndAcesso");
                }

            }
        });

    }

});
