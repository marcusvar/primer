/**
* vtype
*/
Ext.apply(Ext.form.field.VTypes, {
    file: function(val, field) {
        var input, files, file,
            acceptSize = field.acceptSize || 40960, // default max size
            acceptMimes = field.acceptMimes || ['jpg', 'gif', 'png', 'xls', 'xlsx', 'zip', 'rar']; // default types

        input = Ext.get(field.id).down('input'); //<-- 4.0.7
        files = input.getAttribute('files');
        if ( ! files || ! window.FileReader) {
            return true;
        }
        for(var i = 0, l = files.length; i < l; i++) {
            file = files[i];
            if(file.size > acceptSize * 1024) {
                this.fileText = (file.size / 1048576).toFixed(1) + ' MB: tamanho de arquivo inválido ('+(acceptSize / 1024).toFixed(1)+' MB max)';
                return false;
            }
            var ext = file.name.substring(file.name.lastIndexOf('.') + 1);
            if(Ext.Array.indexOf(acceptMimes, ext) === -1) {
                this.fileText = 'Tipo de extensão inválida ('+ext+')';
                return false;
            }
        }
        return true;
    }
});