/*
 * File: app/controller/ctrlTrocarSenha.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Primer.controller.ctrlTrocarSenha', {
    extend: 'Ext.app.Controller',

    onBtnTrocarSenhaClick: function(button, e, eOpts) {
        var form = button.up('form'),
            values = form.getValues(),
            el = form.getEl();

        if(form.getForm().isValid()){
            form.getForm().findField('nova').setValue(global.sha1(values.nova));
            form.getForm().findField('repetir').setValue(form.getForm().findField('nova').getValue());
            form.getEl().mask("Salvando nova senha... favor aguarde...", 'carregando');
            form.getForm().submit({
                url: 'data/senha/alterarSenha.php',
                success: function(form, action){
                    el.unmask();
                    Ext.Msg.alert('Mensagem', 'Alteração realizada com sucesso');
                    form.reset();
                },
                failure: function(form, action) {
                    Ext.Msg.alert('Atenção',Ext.decode(action.response.responseText).msg);
                    el.unmask();
                }
            });
        } else {
            Ext.Msg.alert('Aten&ccedil;&atilde;o','Preencha corretamente todos os campos obrigatórios!');
            return false;
        }
    },

    onFrmTrocarSenhaAfterRender: function(component, eOpts) {
        component.getForm().findField('id').setValue(global.id);
    },

    init: function(application) {
        this.control({
            "button#btnTrocarSenha": {
                click: this.onBtnTrocarSenhaClick
            },
            "form#frmTrocarSenha": {
                afterrender: this.onFrmTrocarSenhaAfterRender
            }
        });
    }

});
