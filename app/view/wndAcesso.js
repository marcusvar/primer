/*
 * File: app/view/wndAcesso.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Primer.view.wndAcesso', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Text',
        'Ext.toolbar.Toolbar',
        'Ext.toolbar.Fill',
        'Ext.button.Button'
    ],

    autoShow: true,
    border: false,
    draggable: false,
    height: 245,
    id: 'wndAcesso',
    width: 360,
    resizable: false,
    closable: false,
    iconCls: 'acesso',
    title: 'Acesso ao sistema',
    expandOnShow: false,
    modal: true,
    plain: true,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    baseCls: 'login',
                    border: false,
                    height: 84
                },
                {
                    xtype: 'form',
                    flex: 1,
                    border: false,
                    id: 'frmAcesso',
                    defaults: {
                        anchor: '90%',
                        allowBlank: false,
                        selectOnFocus: true,
                        msgTarget: 'under',
                        labelWidth: 60,
                        enableKeyEvents: true
                    },
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldLabel: 'Usu&aacute;rio',
                            name: 'user',
                            blankText: 'Digite um nome de usu&aacute;rio'
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldLabel: 'Senha',
                            name: 'password',
                            inputType: 'password',
                            blankText: 'Digite sua senha'
                        }
                    ],
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'bottom',
                            items: [
                                {
                                    xtype: 'tbfill'
                                },
                                {
                                    xtype: 'button',
                                    id: 'btnAcesso',
                                    iconCls: 'cadeado',
                                    text: 'Acesso'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});