/*
 * File: app/view/pnlTrocarSenha.js
 *
 * This file was generated by Sencha Architect version 3.0.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Primer.view.pnlTrocarSenha', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.senha',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Hidden',
        'Ext.form.field.Text',
        'Ext.toolbar.Toolbar',
        'Ext.toolbar.Fill',
        'Ext.button.Button'
    ],

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    id: 'frmTrocarSenha',
                    width: 400,
                    bodyPadding: 10,
                    fieldDefaults: {
                        labelWidth: 120,
                        msgTarget: 'side',
                        labelAlign: 'right',
                        autoFitErrors: false
                    },
                    items: [
                        {
                            xtype: 'hiddenfield',
                            anchor: '100%',
                            name: 'id'
                        },
                        {
                            xtype: 'textfield',
                            validator: function(value) {
                                return this.textValid;
                            },
                            textValid: false,
                            anchor: '100%',
                            fieldLabel: 'Senha atual',
                            name: 'atual',
                            validateOnChange: false,
                            inputType: 'password',
                            invalidText: 'Senha inválida',
                            allowBlank: false,
                            listeners: {
                                blur: {
                                    fn: me.onTextfieldBlur1,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            itemId: 'nova',
                            fieldLabel: 'Nova senha',
                            name: 'nova',
                            inputType: 'password',
                            allowBlank: false,
                            listeners: {
                                validitychange: {
                                    fn: me.onTextfieldValidityChange,
                                    scope: me
                                },
                                blur: {
                                    fn: me.onTextfieldBlur,
                                    scope: me
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            initialPassField: 'nova',
                            anchor: '100%',
                            fieldLabel: 'Repetir nova senha',
                            name: 'repetir',
                            inputType: 'password',
                            allowBlank: false,
                            vtype: 'password'
                        }
                    ],
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'bottom',
                            items: [
                                {
                                    xtype: 'tbfill'
                                },
                                {
                                    xtype: 'button',
                                    id: 'btnTrocarSenha',
                                    iconCls: 'enviar',
                                    text: 'Enviar'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    onTextfieldBlur1: function(component, e, eOpts) {
        if(component.getValue()==="") {
            component.textValid = "";
            return;
        }

        Ext.Ajax.request({
            method: 'POST',
            url: 'data/senha/verificaSenha.php',
            scope: component,
            params: {
                senha: global.sha1(component.getValue()),
                id: global.id
            },
            success: function (response){
                var ret = JSON.parse(response.responseText).success ;
                if (ret){
                    this.clearInvalid();
                    this.textValid = true;
                } else {
                    this.markInvalid('Erro: Senha inválida!');
                    this.textValid = false;
                }
            },
            failure: requestError
        });

    },

    onTextfieldValidityChange: function(field, isValid, eOpts) {
        field.next().validate();
    },

    onTextfieldBlur: function(component, e, eOpts) {
        component.next().validate();
    }

});