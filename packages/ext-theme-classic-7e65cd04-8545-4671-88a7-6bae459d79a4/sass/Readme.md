# ext-theme-classic-7e65cd04-8545-4671-88a7-6bae459d79a4/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    ext-theme-classic-7e65cd04-8545-4671-88a7-6bae459d79a4/sass/etc
    ext-theme-classic-7e65cd04-8545-4671-88a7-6bae459d79a4/sass/src
    ext-theme-classic-7e65cd04-8545-4671-88a7-6bae459d79a4/sass/var
