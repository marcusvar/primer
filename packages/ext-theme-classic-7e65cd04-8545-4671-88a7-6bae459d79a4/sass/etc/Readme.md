# ext-theme-classic-7e65cd04-8545-4671-88a7-6bae459d79a4/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-classic-7e65cd04-8545-4671-88a7-6bae459d79a4/sass/etc"`, these files
need to be used explicitly.
