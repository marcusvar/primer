# ext-theme-neptune-26915fb2-79ff-43b3-b8e0-a162e32c1719/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-neptune-26915fb2-79ff-43b3-b8e0-a162e32c1719/sass/etc"`, these files
need to be used explicitly.
