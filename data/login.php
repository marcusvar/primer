<?php

require("config/db.php");

session_start();

// login e senha enviada do formulário 
$userName = isset($_POST['user']) ? $_POST['user'] : "fulano"; 
$pass = isset($_POST['password']) ? $_POST['password'] : "d033e22ae348aeb5660fc2140aec35850c4da997"; 

// para proteger o canco contra MySQL injection
$userName = stripslashes($userName);
$pass = stripslashes($pass);

$userName = $mysqli->real_escape_string($userName);
$pass = $mysqli->real_escape_string($pass);
$sql = "SELECT * FROM pmr_usuario WHERE Login='$userName' and Senha='$pass'";

$result = array();

if ($resultdb = $mysqli->query($sql)) {

	// determina o numero de linhas retonadas pelo result set
	$count = $resultdb->num_rows;
	// Se retornar pelo menos uma linha
	if($count==1){ 

        $ret = $resultdb->fetch_assoc();

		$_SESSION['authenticated'] = "yes";
	    $_SESSION['username']      = $userName;
        $_SESSION['Id']            = $ret['Id'];
        $_SESSION['Funcao']        = $ret['Funcao'];
		
		$result['success'] = true;
		$result['msg']    = 'Usuário autenticado!';
        $result['Id']     = $ret['Id'];
        $result['User']   = $userName;
        $result['Funcao'] = $ret['Funcao'];
        $result['Data']   = date('Y-m-d\TH:i:s'); 

	} else {
		$result['success'] = false;
		$result['msg'] = 'Login ou senha incorretos.';
	}

	/* fecha result set */
	$resultdb->close();
}

/* encerra a conexão */
$mysqli->close();

//JSON encoding
echo json_encode($result);
?>
