<?php 
require("config/db.php");

$query = "SELECT * FROM pmr_uf";

//consulta sql
$result = $mysqli->query($query);

$retorno = array();
while ($row = $result->fetch_assoc()) {
    $retorno["estados"][] = array( "Id" => $row["Id"], 
                                   "Nome" => $row["Nome"],
                                   "Sigla" => $row["Sigla"]);
}
echo json_encode($retorno);
?>
