<?php
session_start();
$id     = $_SESSION['Id'];
$funcao = $_SESSION['Funcao'];
$user   = $_SESSION['username'];
if(isset($_SESSION['authenticated'])) {
    echo '{ "success": true, "msg": "Acesso autorizado!", "Id": "'.$id.'", "Funcao": "'.$funcao.'", "User": "'.$user.'", "Data":"'.date('Y-m-d\TH:i:s').'" }';
} else {
    echo '{ "success": false, "msg": "Acesso negado!" }';
    return;
}    
?>