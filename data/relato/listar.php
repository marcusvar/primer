<?php

require("../config/db.php");

$id = $_REQUEST['ofertaId'];

$queryString = "
SELECT 
  pmr_relato.Id, pmr_usuario.Nome, DataHora, Relato
FROM
  pmr_relato
JOIN pmr_usuario ON pmr_relato.UsuarioId = pmr_usuario.Id
WHERE
  OfertaId = $id
ORDER BY DataHora DESC";

//consulta sql
$result = $mysqli->query($queryString);

//faz um looping e cria um array com os campos da consulta
$relatos = array();
while($relato = $result->fetch_assoc()) {
    $relatos[] = $relato;
}

//encoda para formato JSON
echo json_encode(array(
    "success" => $mysqli->errno == 0,
    "msg" => $mysqli->error,
    "relato" => $relatos
));
?>