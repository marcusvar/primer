<?php
require("../config/db.php");

require_once("../chromephp/ChromePhp.php");

ChromePhp::log($_REQUEST);

$info = $_REQUEST['relato'];

$data = json_decode($info);

ChromePhp::log($data);

//consulta sql
$query = sprintf(
        "INSERT INTO pmr_relato (Relato, UsuarioId, OfertaId) values ('%s', %s, %s)",
$mysqli->real_escape_string($data->Relato),
$mysqli->real_escape_string($data->UsuarioId),
$mysqli->real_escape_string($data->OfertaId));

$mysqli->query($query);

$errono = $mysqli->errno == 0;
$msg    = $mysqli->error;
$id     = $mysqli->insert_id;

ChromePhp::log($query);

$query = "SELECT 
  pmr_relato.Id, pmr_usuario.Nome, Relato, DataHora
FROM
  pmr_relato
JOIN pmr_usuario ON pmr_relato.UsuarioId = pmr_usuario.Id
WHERE
  OfertaId = $id";

ChromePhp::log($query);

$result = $mysqli->query($query);

$relato = $result->fetch_assoc();

echo json_encode(array(
    "success" => $errono,
    "msg" => $msg,
    "relato" => array(
        "Id" => $id,
        "Nome" => $relato->Nome,
        "Relato" => $relato->Relato,
        "Data" => $relato->Data
    )
));