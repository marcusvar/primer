<?php
require("../config/db.php");

$info = $_POST['usuarios'];
//$info = '{"Id":"0","Nome":"Novo teste","Nacionalidade":"","Naturalidade":"","Nascimento":"","Mae":"","Pai":"","Profissao":"","EstadoCivil":"","Endereco":"","Cidade":"","UF":"","RG":"","CPF":"","Email":"","Login":"novoteste2","Senha":"40bd001563085fc35165329ea1ff5c5ecbdbbeef","Funcao":"","RegiaoLocalidadeId":"","UFRegiao":""}';

$data = json_decode($info);

//consulta sql
$query = sprintf(
        "INSERT INTO pmr_usuario (Nome, Nacionalidade, Naturalidade, ".
        "                         Nascimento, Mae, Pai, Profissao, EstadoCivil, ".
        "                         Endereco, Cidade, UF, RG, CPF, Email, Login, Senha, Funcao, ".
        "                         RegiaoLocalidadeId) ".
        "   values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', ".
        "           '%s', '%s', '%s', '%s', '%s', %s)",
    $mysqli->real_escape_string($data->Nome),
    $mysqli->real_escape_string($data->Nacionalidade),
    $mysqli->real_escape_string($data->Naturalidade),
    $mysqli->real_escape_string($data->Nascimento ),
    $mysqli->real_escape_string($data->Mae),
    $mysqli->real_escape_string($data->Pai),
    $mysqli->real_escape_string($data->Profissao),
    $mysqli->real_escape_string($data->EstadoCivil ),
    $mysqli->real_escape_string($data->Endereco),
    $mysqli->real_escape_string($data->Cidade),
    $mysqli->real_escape_string($data->UF),
    $mysqli->real_escape_string($data->RG),
    $mysqli->real_escape_string($data->CPF),
    $mysqli->real_escape_string($data->Email),
    $mysqli->real_escape_string($data->Login),
    $mysqli->real_escape_string($data->Senha),
    $mysqli->real_escape_string($data->Funcao),
    $mysqli->real_escape_string($data->RegiaoLocalidadeId === "" ? "NULL" : $data->RegiaoLocalidadeId ));

$mysqli->query($query);

echo json_encode(array(
    "success" => $mysqli->errno == 0,
    "usuarios" => array(
        "Id" => $mysqli->insert_id,
        "Nome" => $data->Nome,
        "Email" => $data->Email,
        "Login" => $data->Login,
        "Funcao" => $data->Senha
    )
));
?>