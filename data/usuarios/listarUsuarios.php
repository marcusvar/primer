<?php

require("../config/db.php");

$start = $_REQUEST['start']; //isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
$limit = $_REQUEST['limit']; //isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 25;

$queryString = "SELECT * FROM pmr_usuario ORDER BY Nome ".
               "LIMIT $start,  $limit";

//consulta sql
$result = $mysqli->query($queryString);

//faz um looping e cria um array com os campos da consulta
$usuarios = array();
while($usuario = $result->fetch_assoc()) {
    $usuarios[] = $usuario;
}

//consulta total de linhas na tabela
$queryTotal = $mysqli->query('SELECT count(*) as num FROM pmr_usuario') or die(mysql_error());
$row = $queryTotal->fetch_assoc();
$total = $row['num'];

//encoda para formato JSON
echo json_encode(array(
    "success" => mysql_errno() == 0,
    "total" => $total,
    "usuarios" => $usuarios
));
?>