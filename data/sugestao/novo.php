<?php
require("../config/db.php");

//require_once("../chromephp/ChromePhp.php");

//ChromePhp::log($_REQUEST);


$info = $_REQUEST['sugestao'];

$data = json_decode($info);

//ChromePhp::log($data);

//consulta sql
$query = sprintf(
        "INSERT INTO pmr_sugestao (Valor, UsuarioId, OfertaId) values (%s, %s, %s)",
$mysqli->real_escape_string($data->Valor),
$mysqli->real_escape_string($data->UsuarioId),
$mysqli->real_escape_string($data->OfertaId));

$mysqli->query($query);

$errono = $mysqli->errno == 0;
$msg    = $mysqli->error;
$id     = $mysqli->insert_id;

$query = "SELECT 
  pmr_sugestao.Id, pmr_usuario.Nome, pmr_usuario.Funcao, Valor, DataHora
FROM
  pmr_sugestao
JOIN pmr_usuario ON pmr_sugestao.UsuarioId = pmr_usuario.Id
WHERE
  OfertaId = $id";

$result = $mysqli->query($query);

$oferta = $result->fetch_assoc();

echo json_encode(array(
    "success" => $errono,
    "msg" => $msg,
    "ofertas" => array(
        "Id" => $id,
        "Nome" => $oferta->Nome,
        "Funcao" => $oferta->Funcao,
        "Valor" => $oferta->Valor,
        "Data" => $oferta->Data
    )
));