<?php

require("../config/db.php");

//$id = isset($_REQUEST['ofertaId']) ? $_REQUEST['ofertaId'] : 13;
$id = $_REQUEST['ofertaId'];
        
$queryString = "
SELECT 
  pmr_sugestao.Id, pmr_usuario.Nome, pmr_usuario.Funcao, Valor, DataHora
FROM
  pmr_sugestao
JOIN pmr_usuario ON pmr_sugestao.UsuarioId = pmr_usuario.Id
WHERE
  OfertaId = $id
ORDER BY DataHora DESC";

//consulta sql
$result = $mysqli->query($queryString);

//faz um looping e cria um array com os campos da consulta
$sugestoes = array();
while($sugestao = $result->fetch_assoc()) {
    $sugestoes[] = $sugestao;
}

//encoda para formato JSON
echo json_encode(array(
    "success" => $mysqli->errno == 0,
    "msg" => $mysqli->error,
    "sugestao" => $sugestoes
));
?>