<?php
require("../config/db.php");

//require_once("../chromephp/ChromePhp.php");

//ChromePhp::log($_REQUEST);

$info = $_REQUEST['comentario'];

$data = json_decode($info);

//ChromePhp::log($data);

//consulta sql
$query = sprintf(
        "INSERT INTO pmr_comentario (Comentario, UsuarioId, OfertaId) values ('%s', %s, %s)",
$mysqli->real_escape_string($data->Comentario),
$mysqli->real_escape_string($data->UsuarioId),
$mysqli->real_escape_string($data->OfertaId));

$mysqli->query($query);

$errono = $mysqli->errno == 0;
$msg    = $mysqli->error;
$id     = $mysqli->insert_id;

//ChromePhp::log($query);

$query = "SELECT 
  pmr_comentario.Id, pmr_usuario.Nome, Comentario, DataHora
FROM
  pmr_comentario
JOIN pmr_usuario ON pmr_comentario.UsuarioId = pmr_usuario.Id
WHERE
  OfertaId = $id";

//ChromePhp::log($query);

$result = $mysqli->query($query);

$comentario = $result->fetch_assoc();

echo json_encode(array(
    "success" => $errono,
    "msg" => $msg,
    "comentario" => array(
        "Id" => $id,
        "Nome" => $comentario->Nome,
        "Comentario" => $comentario->Comentario,
        "Data" => $comentario->Data
    )
));