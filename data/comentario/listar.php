<?php

require("../config/db.php");

$id = $_REQUEST['ofertaId'];

$queryString = "
SELECT 
  pmr_comentario.Id, pmr_usuario.Nome, DataHora, Comentario
FROM
  pmr_comentario
JOIN pmr_usuario ON pmr_comentario.UsuarioId = pmr_usuario.Id
WHERE
  OfertaId = $id
ORDER BY DataHora DESC";

//consulta sql
$result = $mysqli->query($queryString);

//faz um looping e cria um array com os campos da consulta
$comentarios = array();
while($comentario = $result->fetch_assoc()) {
    $comentarios[] = $comentario;
}

//encoda para formato JSON
echo json_encode(array(
    "success" => $mysqli->errno == 0,
    "msg" => $mysqli->error,
    "comentario" => $comentarios
));
?>