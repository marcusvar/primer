<?php

require("../config/db.php");

$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 25;

$queryString = "SELECT pmr_oferta.*, ".
               "pl.UFId, pl.Nome AS Cidade, pu.Sigla AS Uf FROM pmr_oferta ".
               "JOIN pmr_localidade pl ON pmr_oferta.LocalidadeId = pl.Id ".
               "JOIN pmr_uf pu ON pl.UFId = pu.Id ".
               "ORDER BY PrimeiraPraca ASC ".
               "LIMIT $start,  $limit";

//consulta sql
$result = $mysqli->query($queryString);

//faz um looping e cria um array com os campos da consulta
$ofertas = array();
while($usuario = $result->fetch_assoc()) {
    $usuario["dtPrimeiraPraca"] = $usuario["PrimeiraPraca"]."T".$usuario["PrimeiraPracaHora"];
    $usuario["dtSegundaPraca"]  = $usuario["SegundaPraca"]."T".$usuario["SegundaPracaHora"];
    $usuario["PrimeiraPracaHora"] = substr($usuario["PrimeiraPracaHora"],0,5);
    $usuario["SegundaPracaHora"]  = substr($usuario["SegundaPracaHora"],0,5);
    $ofertas[] = $usuario;
}

//consulta total de linhas na tabela
$queryTotal = $mysqli->query('SELECT count(*) as num FROM pmr_oferta') or die(mysql_error());
$row = $queryTotal->fetch_assoc();
$total = $row['num'];

//encoda para formato JSON
echo json_encode(array(
    "success" => mysql_errno() == 0,
    "total" => $total,
    "ofertas" => $ofertas
));
