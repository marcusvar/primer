<?php
require("../config/db.php");

//require_once("../chromephp/ChromePhp.php");

//ChromePhp::log($_REQUEST);
//ChromePhp::log($_POST);
//ChromePhp::log($_FILES);

//$info = $_POST['ofertas'];

$info = isset($_POST['ofertas']) ? $_POST['ofertas'] :
        '{"Id":"13","Descricao":"Descri\u00e7\u00e3o teste11","Situacao":"Vendido","Processo":"98897987987987","Executado":"sadffsaef","Bem":"kjgjkhkjh","Tipo":"kjhkjhjkh","LocalidadeId":"94","Uf":"AC","Localizacao":"kjgkjhkjg","TipoDocumento":"jhgjhghjg","Pendencias":"ksjdfkajs","Justica":"jhgjhgjhgjh","Vara":"jhgjhgjhg","Leiloeiro":"jhgjhghjg","Comissao":"5","LocalLeilao":"jhgjhghjg","PrimeiraPraca":"2014-02-20","PrimeiraPracaHora":"08:45","LanceMinimo1":"98798.89","SegundaPraca":"2014-02-28","SegundaPracaHora":"17:30","LanceMinimo2":"89798.77","Observacoes":"kjgkgkjgkjg","Edital":"","UsuarioId":"2","Cidade":"Rio Branco","UFId":"1","dtPrimeiraPraca":"2014-02-20T08:45:00","dtSegundaPraca":"2014-02-28T17:30:00"}';

$nomePdf = '';

if($_FILES['pdf']['name'] !== '') {
    $nomePdf = md5(uniqid(time())).'.pdf';
    @move_uploaded_file($_FILES['pdf']['tmp_name'], '../../upload/'.$nomePdf);
}

//$data = json_decode($info);
$data = json_decode(json_encode($_POST), FALSE);

//consulta sql
$query = sprintf(
"UPDATE pmr_oferta
 SET Descricao         = '%s',
     Situacao          = '%s',
     Processo          = '%s',
     Executado         = '%s',
     Exequente         = '%s',
     Bem               = '%s',
     Tipo              = '%s',
     LocalidadeId      = %s,
     Localizacao       = '%s',
     TipoDocumento     = '%s',
     Pendencias        = '%s',
     Justica           = '%s',
     Vara              = '%s',
     Leiloeiro         = '%s',
     Comissao          = %s,
     LocalLeilao       = '%s',
     PrimeiraPraca     = '%s',
     PrimeiraPracaHora = '%s',
     LanceMinimo1      = '%s',
     SegundaPraca      = '%s',
     SegundaPracaHora  = '%s',
     LanceMinimo2      = '%s',
     Observacoes       = '%s',
     Edital            = '%s',
     UsuarioId         = %s        
 WHERE Id = %s",
$mysqli->real_escape_string($data->Descricao),
$mysqli->real_escape_string($data->Situacao),
$mysqli->real_escape_string($data->Processo),
$mysqli->real_escape_string($data->Executado),
$mysqli->real_escape_string($data->Exequente),
$mysqli->real_escape_string($data->Bem),
$mysqli->real_escape_string($data->Tipo),
$data->LocalidadeId,
$mysqli->real_escape_string($data->Localizacao),
$mysqli->real_escape_string($data->TipoDocumento),
$mysqli->real_escape_string($data->Pendencias),
$mysqli->real_escape_string($data->Justica),
$mysqli->real_escape_string($data->Vara),
$mysqli->real_escape_string($data->Leiloeiro),
$mysqli->real_escape_string($data->Comissao),
$mysqli->real_escape_string($data->LocalLeilao),
$mysqli->real_escape_string($data->PrimeiraPraca),
$mysqli->real_escape_string($data->PrimeiraPracaHora),
$mysqli->real_escape_string($data->LanceMinimo1),
$mysqli->real_escape_string($data->SegundaPraca),
$mysqli->real_escape_string($data->SegundaPracaHora),
$mysqli->real_escape_string($data->LanceMinimo2),
$mysqli->real_escape_string($data->Observacoes),
$nomePdf,
$data->UsuarioId,
$data->Id);

$mysqli->query($query);

echo json_encode(array(
    "success" => $mysqli->errno == 0,
    "ofertas" => array(
        "Id" => $data->Id,
        "PrimeiraPraca" => $data->PrimeiraPraca,
        "SegundaPraca" => $data->SegundaPraca,
        "Descricao" => $data->Descricao,
        "Cidade" => $data->Cidade,
        "UF" => $data->Uf
    )
));
