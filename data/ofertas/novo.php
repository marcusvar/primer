<?php
require("../config/db.php");

//require_once("../chromephp/ChromePhp.php");

//ChromePhp::log($_REQUEST);
//ChromePhp::log($_POST);
//ChromePhp::log($_FILES);

$nomePdf = '';

if($_FILES['pdf']['name'] !== '') {
    $nomePdf = md5(uniqid(time())).'.pdf';
    @move_uploaded_file($_FILES['pdf']['tmp_name'], '../../upload/'.$nomePdf);
}
//$_POST['ofertas'] = isset($_POST['ofertas']) ? $_POST['ofertas'] :
//'{"Id":"1","Descricao":"Terreno da Avenida das Freiras 10","Situacao":"Participou com lance derrotado","Processo":"28736482","Executado":"Teste executado","Bem":"Im\u00f3vel","Tipo":"Urbano","LocalidadeId":"4369","Uf":"RO","Localizacao":"Av. Das Freiras numero 5300, Bairro Embratel","TipoDocumento":"Escritura P\u00fablica","Pendencias":"Encontra-se com um ano de IPTU atrazaso.","Justica":"Justi\u00e7a do Trabalho","Vara":"Terceira Vara","Leiloeiro":"Lauiz Gabriel Samuel","Comissao":"","LocalLeilao":"Av das Na\u00e7oes numero 300, (tribubal do trabalho)","PrimeiraPraca":"2014-02-12","PrimeiraPracaHora":"14:00","LanceMinimo1":"19000.00","SegundaPraca":"2014-02-27","SegundaPracaHora":"17:00","LanceMinimo2":"11400.00","Observacoes":"http://www.bdmg.mg.gov.br/Lists/cadastro_licitacoes/Attachments/90/Edital%20038-2011%20-%20Leil%C3%A3o%20-%20im%C3%B3veis%20-%20DAP.pdf","Edital":"Im\u00f3vel sem benfeitorias, com localiza\u00e7\u00e3o razoavel, boa topografia, inpostos municipais em dia. O terreno fica em frente ao Super Mercado \"Verdura Grande\"","UsuarioId":"2","Cidade":"Ji-Paran\u00e1","UFId":"21","dtPrimeiraPraca":"2014-02-12T14:00:00","dtSegundaPraca":"2014-02-27T17:00:00"}';

$info = $_POST['ofertas'];

//ChromePhp::info('$info');
//ChromePhp::log($info);

$data = json_decode(json_encode($_POST), FALSE); //json_decode($info);
//$data = json_decode($info);
        
//ChromePhp::info('$data');
//ChromePhp::log($data);

//consulta sql
$query = sprintf(
        "INSERT INTO pmr_oferta (Descricao, Situacao, Processo, Executado, Exequente, Bem, Tipo, LocalidadeId, ".
        "                        Localizacao, TipoDocumento, Pendencias, Justica, Vara, Leiloeiro, Comissao, ".
        "                        LocalLeilao, PrimeiraPraca, PrimeiraPracaHora, LanceMinimo1, ".
        "                        SegundaPraca, SegundaPracaHora, LanceMinimo2, Observacoes, Edital, UsuarioId) ".
        "   values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', ".
        "           %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s)",
$mysqli->real_escape_string($data->Descricao),
$mysqli->real_escape_string($data->Situacao),
$mysqli->real_escape_string($data->Processo),
$mysqli->real_escape_string($data->Executado),
$mysqli->real_escape_string($data->Exequente),
$mysqli->real_escape_string($data->Bem),
$mysqli->real_escape_string($data->Tipo),
$data->LocalidadeId,
$mysqli->real_escape_string($data->Localizacao),
$mysqli->real_escape_string($data->TipoDocumento),
$mysqli->real_escape_string($data->Pendencias),
$mysqli->real_escape_string($data->Justica),
$mysqli->real_escape_string($data->Vara),
$mysqli->real_escape_string($data->Leiloeiro),
$data->Comissao,
$mysqli->real_escape_string($data->LocalLeilao),
$mysqli->real_escape_string($data->PrimeiraPraca),
$mysqli->real_escape_string($data->PrimeiraPracaHora),
$mysqli->real_escape_string($data->LanceMinimo1),
$mysqli->real_escape_string($data->SegundaPraca),
$mysqli->real_escape_string($data->SegundaPracaHora),
$mysqli->real_escape_string($data->LanceMinimo2),
$mysqli->real_escape_string($data->Observacoes),
$nomePdf,
$data->UsuarioId);

//ChromePhp::log($query);

$mysqli->query($query);

echo json_encode(array(
    "success" => $mysqli->errno == 0,
    "ofertas" => array(
        "Id" => $mysqli->insert_id,
        "PrimeiraPraca" => $data->PrimeiraPraca,
        "SegundaPraca" => $data->SegundaPraca,
        "Descricao" => $data->Descricao,
        "Cidade" => $data->Cidade,
        "UF" => $data->Uf
    )
));
